#include <iostream>
#include <conio.h>

using namespace std;

//enums for rank and suit 


enum Rank
{
	//# will be auto assigned after the previous 
	Two = 2, Three, Four, Five, Six, Seven, Eight,
	Nine, Ten, Jack, Queen, King, Ace,
};

enum Suit
{
	Spades, Hearts, Clubs, Diamonds
};
//struct for cards
struct Card
{
	Rank rank;
	Suit suit;
};



int main()
{

	(void)_getch();
	return 0;
}